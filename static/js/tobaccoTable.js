var table = Vue.mixin({
  data: {
        sortKey: '',
        sortOrders: {}      
    },
  props: {
    data: Array,
    columns: Array,
    filterKey: String,
    filterColumn: Object
  },
  computed: {
    filteredData: function () {
      let sortKey = this.sortKey
      let filterKey = this.filterKey && this.filterKey.toLowerCase()
      let data = this.data
      let order = this.sortOrders[sortKey] || 1
      let filterColumn = this.filterColumn

      for (column in filterColumn){
        console.log(filterColumn)
        if (filterColumn[column].length>=1) {          
          data = data.filter(function(brand) {
            return filterColumn[column].indexOf(brand[column]) > -1
          })
        }
      }
      if (filterKey) {
        data = data.filter(function (row) {
          return Object.keys(row).some(function (key) {
            return String(row[key]).toLowerCase().indexOf(filterKey) > -1
          })
        })
      }
      if (sortKey) {
        data = data.slice().sort(function (a, b) {
          a = a[sortKey]
          b = b[sortKey]
          return (a === b ? 0 : a > b ? 1 : -1) * order
        })
      }
      return data
    }
  },
  filters: {
    capitalize: function (str) {
      return str.charAt(0).toUpperCase() + str.slice(1)
    }
  },
  methods: {
    sortBy: function (key) {
      this.sortKey = key
      this.sortOrders[key] = this.sortOrders[key] * -1
    }
  }
})
// register the grid component
Vue.component('grid', {
    template: `
<div class="d-none d-sm-block"> 
    <table class="border table table-striped shadow table-white">
        <thead class="thead-dark"> 
        <tr>
            <th v-for="key in columns"
            @click="sortBy(key)"
            :class="{ active: sortKey == key }">
            {{ key }}
            <span class="arrow" :class="sortOrders[key] > 0 ? 'asc' : 'dsc'"></span>
            </th>
        </tr>
        </thead>
        <tbody>
        <tr v-for="entry in filteredData">
            <td v-for="key,index in columns">          
            <template v-if="key==='Вкус'">
                <a :href="'/tobacco/get?brand='+entry['Брэнд']+'&taste='+entry['Вкус']"> {{entry[key]}} </a>
            </template>
            <template v-else-if="key==='Оценка' && Number.isFinite(entry['Оценка'])">
              {{entry['Оценка'].toFixed(1)}}
            </template>  
            <template v-else>
                {{entry[key]}}
            </template>          
            </td>
        </tr>
        </tbody>
    </table>
    <div class="d-flex justify-content-center pt-5"> 
    <svg v-if="data.length===0" class="spinner justify-content-center" width="65px" height="65px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg">
      <circle class="path" fill="none" stroke-width="6" stroke-linecap="round" cx="33" cy="33" r="30"></circle>
    </svg>
  </div>
  </div>
 `,
    data: function () {
      var sortOrders = {}
      this.columns.forEach(function (key) {
        sortOrders[key] = 1
      })
      return {
        sortKey: '',
        sortOrders: sortOrders
      }
    },
    mixins: [table]
  })
  
  Vue.component('grid-mobile', {
    template: `
<div class="d-block d-sm-none"> 
    <table class="border table table-striped shadow table-white">
      <thead class="thead-dark"> 
        <tr>
          <th v-for="key in mobileColumns"
          @click="sortBy(key)"
          :class="{ active: sortKey == key }">
          {{ key }}
          <span class="arrow" :class="sortOrders[key] > 0 ? 'asc' : 'dsc'"></span>
          </th>
        </tr>
      </thead>
      <tbody>
        <template v-for="entry in filteredData">
        <tr>
          <td v-for="key,index in mobileColumns">
              <span v-if="index===0" @click="entry['show']=!entry['show']">+</span>          
              <template v-if="key==='Вкус'">
                  <a :href="'/tobacco/get?brand='+entry['Брэнд']+'&taste='+entry['Вкус']"> {{entry[key]}} </a>
              </template>
              <template v-else-if="key==='Оценка' && Number.isFinite(entry['Оценка'])">
                {{entry['Оценка'].toFixed(1)}}
              </template>              
              <template v-else>
                  {{entry[key]}}
              </template>          
            </td>
          </tr>
            <td colspan="2" v-if="entry['show']">
              <table class="footable-details border table table-striped shadow table-white">
                  <tbody>
                      <tr>
                          <th>Брэнд</th>
                          <td style="display: table-cell;">{{entry['Брэнд']}}</td>
                      </tr>
                      <tr>
                          <th>Вид вкуса</th>
                          <td style="display: table-cell;">{{entry['Вид вкуса']}}</td>
                      </tr>
                  </tbody>
              </table>
          </td>          
          </tr>
        </template> 
      </tbody>
    </table>
    <div class="d-flex justify-content-center pt-5"> 
    <svg v-if="data.length===0" class="spinner justify-content-center" width="65px" height="65px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg">
      <circle class="path" fill="none" stroke-width="6" stroke-linecap="round" cx="33" cy="33" r="30"></circle>
    </svg>
  </div>
</div>`,
    data: function () {
      var sortOrders = {}
      this.columns.forEach(function (key) {
        sortOrders[key] = 1
      })
      return {
        sortKey: '',
        sortOrders: sortOrders
      }
    },
    mixins: [table],
    props: {
      mobileColumns: Array
      }
  })

  var tobaccoTable = new Vue({
    el: '#tobaccoTable',
    data: {
      brands: [],
      tastes: [],
      searchQuery: '',
      gridColumns: ['Брэнд', 'Вид вкуса', 'Вкус', 'Оценка'],
      gridMobileColumns: ['Вкус', 'Оценка'],
      gridData: []      
    },
    created() {
        current_host = window.location.hostname
        port = window.location.port
        protocol = window.location.protocol
        if (window.location.hostname === '127.0.0.1'){
          url = protocol+'//'+current_host+':'+port+'/api/tobacco/'
        }
        else{ url = protocol+'//'+ current_host+'/api/tobacco/'}       
        fetch(url).then((response) => {
                if(response.ok) {
                    return response.json();
                }            
                throw new Error('Network response was not ok');
            })
            .then((json) => {
                for (key in json) {
                    this.gridData.push({
                        'Брэнд': json[key].brand,
                        'Вид вкуса': json[key].taste_kind,
                        'Вкус': json[key].taste,
                        'Оценка': json[key].avg_rate,
                        'show': false
                    });
                }                
            })
            .catch((error) => {
                console.log(error);
            });
    }
  })