from django.views import View
from django.shortcuts import render_to_response, redirect
from django.template.context_processors import csrf
from django.contrib import auth


class LoginView(View):
    def get(self, request, *args, **kwargs):
        args = {}
        args.update(csrf(request))
        return render_to_response('login.html', args)

    def post(self, request, *args, **kwargs):
        args = {}
        args.update(csrf(request))
        username = request.POST.get('username', '')
        password = request.POST.get('password', '')
        user = auth.authenticate(username=username, password=password)
        if user is not None:
            auth.login(request, user)
            return redirect('/')
        else:
            args['login_error'] = 'Пользователь или пароль неверный'
            return render_to_response('login.html', args)


class LogoutView(View):
    def get(self, request, *args, **kwargs):
        auth.logout(request)
        url = request.META.get('HTTP_REFERER')
        return redirect(url)


class RegisterView(View):
    def get(self, request, *args, **kwargs):
        args = {}
        args.update(csrf(request))
        form = auth.forms.UserCreationForm()
        args['form'] = form
        return render_to_response('register.html', args)

    def post(self, request, *args, **kwargs):
        args = {}
        args.update(csrf(request))
        newuser_form = auth.forms.UserCreationForm(request.POST)
        if newuser_form.is_valid():
            # Запоминаем данные для логина пользователя после регистрации
            user = newuser_form.save()
            password = newuser_form.clean_password2()
            newuser = auth.authenticate(username=user, password=password)
            auth.login(request, newuser)
            return redirect('/')
        else:
            args['form'] = newuser_form
        return render_to_response('register.html', args)
