# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from rest_framework import viewsets
from api.serializers import TobSerializer
from tobacco.models import Tobacco
from django.db.models import Avg


class TobaccoViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = Tobacco.objects.annotate(avg_rate=Avg('tobacco_rating__rating'))
    serializer_class = TobSerializer
