from rest_framework import serializers
from tobacco.models import Tobacco, Tobacco_brand, Tobacco_taste


class TobSerializer(serializers.Serializer):
    brand = serializers.CharField(max_length=200)
    taste_kind = serializers.CharField(max_length=200)
    taste = serializers.CharField(max_length=200)
    avg_rate = serializers.FloatField()

    class Meta:
        model = Tobacco
        fields = ('brand', 'taste', 'taste_kind', 'avg_rate')

    def create(self, validated_data):
        brand_name = validated_data['brand']
        validated_data['brand'] = Tobacco_brand.objects.get(name=brand_name)
        taste_kind = validated_data['taste_kind']
        taste_kind = Tobacco_taste.objects.get(kind=taste_kind)
        validated_data['taste_kind'] = taste_kind
        return Tobacco.objects.create(**validated_data)
