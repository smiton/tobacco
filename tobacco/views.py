import time
from django.views import View
from django.shortcuts import render_to_response, redirect
from tobacco.models import Tobacco, Tobacco_rating
from tobacco.models import Tobacco_brand, Tobacco_taste, Tobacco_coments
from django.template.context_processors import csrf
from django.contrib import auth
from tobacco.forms import TobaccoForm, CommentForm
from django.contrib.auth.models import User


now_time = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())


class TobaccoListView(View):
    def get(self, request, *args, **kwargs):
        args = {}
        args['brands'] = Tobacco_brand.objects.all().order_by('name')
        args['tastes'] = Tobacco_taste.objects.all().order_by('kind')
        args['username'] = auth.get_user(request).username
        return render_to_response('tobacco_list.html', args)

class TobaccoView(View):
    def get_tobacco_id(self, request):
        tobacco_id = 0
        r_get = request.GET
        if r_get.get('brand') and r_get.get('taste'):
            brand = r_get.get('brand')
            taste = r_get.get('taste')
            tobacco = Tobacco.objects.get(brand__name=brand, taste=taste)
            tobacco_id = tobacco.id
        return tobacco_id

    def get(self, request, tobacco_id=None):
        args = {}
        if tobacco_id is None:
            tobacco_id = self.get_tobacco_id(request)
        args['tobacco'] = Tobacco.objects.get(id=tobacco_id)
        args.update(csrf(request))
        args['ratings'] = Tobacco_rating.objects.filter(tobacco_id_id=tobacco_id)
        args['username'] = auth.get_user(request).username
        args['comments'] = Tobacco_coments.objects.filter(tobacco_id=tobacco_id)
        args['comments_form'] = CommentForm
        return render_to_response('tobacco.html', args)

    def post(self, request, tobacco_id=0):
        if tobacco_id == 0:
            tobacco_id = self.get_tobacco_id(request)
        user_id = auth.get_user(request).id
        defaults = {'rating': request.POST['rate']}
        Tobacco_rating.objects.update_or_create(tobacco_id_id=tobacco_id,
                                                user_id_id=user_id,
                                                defaults=defaults,)
        return self.get(request, tobacco_id)


class TobaccoAddView(View):
    def get_args(self, request):
        form = TobaccoForm
        args = {}
        args.update(csrf(request))
        args['form'] = form
        args['brands'] = Tobacco_brand.objects.all()
        args['tastes'] = Tobacco_taste.objects.all()
        args['username'] = auth.get_user(request).username
        return args

    def get(self, request):
        args = self.get_args(request)
        return render_to_response('tobacco_add.html', args)

    def post(self, request):
        args = self.get_args(request)
        res = render_to_response('tobacco_add.html', args)
        form = TobaccoForm(request.POST)
        if form.is_valid():
            tobacco = form.save()
            id = str(tobacco.id)
            res = redirect('/tobacco/get/' + id)
        return res

class AddComment(View):
    def post(self, request, tobacco_id):
        form = CommentForm(request.POST)
        if form.is_valid():
            comment = form.save(commit=False)
            comment.tobacco_id = Tobacco.objects.get(id=tobacco_id)
            comment.date = now_time
            user_id = auth.get_user(request).id
            comment.user_id = User.objects.get(id=user_id)
            form.save()
        url = request.META.get('HTTP_REFERER')
        return redirect(url)


class TobaccoEditView(View):
    @property
    def template(self):
        return 'tobacco_edit.html'

    def template_args(self, request, tobacco_id):
        args = {}
        args.update(csrf(request))
        args['form'] = TobaccoForm
        args['brands'] = Tobacco_brand.objects.all()
        args['tastes'] = Tobacco_taste.objects.all()
        args['tobacco'] = Tobacco.objects.get(id=tobacco_id)
        args['username'] = auth.get_user(request).username
        return args

    def get(self, request, tobacco_id):
        args = self.template_args(request, tobacco_id)
        return render_to_response(self.template, args)

    def post(self, request, tobacco_id):
        tobacco = Tobacco.objects.get(id=tobacco_id)
        form = TobaccoForm(request.POST or None, instance=tobacco)
        if form.is_valid():
            form.save()
            return redirect('/tobacco/get/'+str(tobacco_id))
        args = self.template_args(request, tobacco_id)
        args['form'] = form
        return render_to_response(self.template, args)
