# -*- coding: utf-8 -*-
# Generated by Django 1.11.11 on 2018-06-10 05:47
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('tobacco', '0004_auto_20180610_1547'),
    ]

    operations = [
        migrations.AlterField(
            model_name='tobacco',
            name='taste_kind',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='tobacco.Tobacco_taste'),
        ),
    ]
