# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from tobacco.models import Tobacco,Tobacco_brand, Tobacco_taste, Tobacco_coments

class TobaccoInline(admin.StackedInline):
    model = Tobacco_coments
    extra = 0


class TobaccoAdmin(admin.ModelAdmin):
    list_display = ('id','brand', 'taste')
    inlines = [TobaccoInline]


admin.site.register(Tobacco, TobaccoAdmin)
admin.site.register(Tobacco_brand)
admin.site.register(Tobacco_taste)
