from django.forms import  ModelForm
from tobacco.models import Tobacco, Tobacco_coments

class TobaccoForm(ModelForm):
    class Meta:
        model = Tobacco
        fields = '__all__'

class CommentForm(ModelForm):
    class Meta:
        model = Tobacco_coments
        fields = ['text']