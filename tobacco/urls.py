from django.urls import path
from tobacco import views

urlpatterns = [
    path(r'get', views.TobaccoView.as_view()),
    path(r'get/<int:tobacco_id>', views.TobaccoView.as_view()),
    path(r'edit/<int:tobacco_id>', views.TobaccoEditView.as_view()),
    path(r'add', views.TobaccoAddView.as_view()),
    path(r'addcoment/<int:tobacco_id>', views.AddComment.as_view()),
    path(r'', views.TobaccoListView.as_view()),
]
