# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.auth.models import User
from django.db import models
from datetime import datetime

# Create your models here.


class Tobacco_brand(models.Model):
    class Meta():
        db_table = "tobacco_brand"
    name = models.CharField(max_length=200)

    def __str__(self):
        return '%s' % (self.name)


class Tobacco_taste(models.Model):
    class Meta():
        db_table = "tobacco_kind"
    kind = models.CharField(max_length=200)

    def __str__(self):
        return '%s' % (self.kind) 


class Tobacco(models.Model):
    class Meta():
        db_table = "tobacco"
        unique_together = (('brand', 'taste'),)
    brand = models.ForeignKey(Tobacco_brand, on_delete=models.CASCADE)
    taste_kind = models.ForeignKey(Tobacco_taste, on_delete=models.CASCADE)
    taste = models.CharField(max_length=200)    
    img_src = models.URLField(blank=True)
    description = models.TextField(blank=True)

    def __str__(self):
        return '%s %s' % (self.brand, self.taste)


class Tobacco_rating(models.Model):
    class Meta():
        db_table = "tobacco_rating"
        unique_together = (('tobacco_id', 'user_id'),)
    tobacco_id = models.ForeignKey(Tobacco, on_delete=models.CASCADE)
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)    
    rating = models.IntegerField()


class Tobacco_coments(models.Model):
    class Meta():
        db_table = 'tobacco_coments'
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    text = models.TextField()
    tobacco_id = models.ForeignKey(Tobacco, on_delete=models.CASCADE)
    date = models.DateTimeField(default=datetime(1970, 1, 1))    