from time import strftime, localtime
from django.views import View
from django.http.response import Http404
from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import render_to_response, redirect
from article.models import Article, Comments
from django.contrib.auth.models import User
from article.forms import CommentForm, ArticleForm
from django.template.context_processors import csrf
from django.contrib import auth
from django.core.paginator import Paginator

now_time = strftime("%Y-%m-%d %H:%M:%S", localtime())


class Articles(View):
    def get(self, request, *args, **kwargs):
        page_number = kwargs.get('page', 1)
        all_articles = Article.objects.all().order_by('-article_date')
        paginator = Paginator(all_articles, 2)
        page = paginator.page(page_number)
        args = {}
        args['articles'] = page
        args['username'] = auth.get_user(request).username
        args['pages'] = paginator.page_range
        return render_to_response('articles.html', args)


class ArticleView(View):
    def get(self, request, article_id=1):
        coment_form = CommentForm
        args = {}
        args.update(csrf(request))
        args['article'] = Article.objects.get(id=article_id)
        args['comments'] = Comments.objects.filter(comments_article=article_id)
        args['form'] = coment_form
        args['username'] = auth.get_user(request).username
        return render_to_response('article.html', args)


class AddLike(View):
    def get(self, request, article_id, *args, **kwargs):
        try:
            if str(article_id) in request.COOKIES:
                return redirect('/')
            else:
                article = Article.objects.get(id=article_id)
                article.article_likes += 1
                article.save()
                response = redirect('/')
                response.set_cookie(str(article_id), True)
                return response
        except ObjectDoesNotExist:
            raise Http404
        return redirect('/')


class AddComment(View):
    def get(self, request, article_id, *args, **kwargs):
        url = request.META.get('HTTP_REFERER')
        return redirect(url)

    def post(self, request, article_id, *args, **kwargs):
        if 'pause' not in request.session:
            form = CommentForm(request.POST)
            if form.is_valid():
                comment = form.save(commit=False)
                comment.comments_article = Article.objects.get(id=article_id)
                comment.comments_date = now_time
                user_id = auth.get_user(request).id
                comment.comments_user = User.objects.get(id=user_id)
                form.save()
                request.session.set_expiry(60)
                request.session['pause'] = True
        url = request.META.get('HTTP_REFERER')
        return redirect(url)


class AddArticle(View):
    def get(self, request, *args, **kwargs):
        form = ArticleForm
        args = {}
        args.update(csrf(request))
        args['form'] = form
        args['username'] = auth.get_user(request).username
        return render_to_response('add_article.html', args)

    def post(self, request, *args, **kwargs):
        form = ArticleForm(request.POST)
        if form.is_valid():
            article = form.save(commit=False)
            article.article_date = now_time
            form.save()
        return redirect('/')
