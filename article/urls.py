from django.urls import path
from article import views

urlpatterns = [
    path(r'article/get/<int:article_id>', views.ArticleView.as_view()),
    path(r'article/addlike/<int:article_id>', views.AddLike.as_view()),
    path(r'article/addcoment/<int:article_id>', views.AddComment.as_view()),
    path(r'article/add', views.AddArticle.as_view()),
    path(r'page/<int:page>', views.Articles.as_view()),
    path(r'', views.Articles.as_view()),
]
